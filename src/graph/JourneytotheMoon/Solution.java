package graph.JourneytotheMoon;

import java.io.*;
import java.util.*;

public class Solution {

    static class Vertex{
        int num;
        boolean isVisited;

        public Vertex(int num) {
            this.num = num;
            isVisited = false;
        }
    }

    static class Graph{
        Vertex[] vertexList;
//        int[][] adjMatrix;
        List<Integer>[] adjList;

         Graph(int n){
             vertexList = new Vertex[n];
//            adjMatrix = new int[n][n];

//            for (int i = 0; i < n; i++) {
//                vertexList[i]=new Vertex(i);
//                for (int j = 0; j < n; j++) {
//                    adjMatrix[i][j]=0;
//                }
//            }
            adjList = new ArrayList[n];
             for (int i = 0; i < n; i++) {
                 vertexList[i]=new Vertex(i);
                 adjList[i] = new ArrayList<>();
             }
        }

        public void addEdge(int a, int b){
//            adjMatrix[a][b]=1;
//            adjMatrix[b][a]=1;

            adjList[a].add(b);
            adjList[b].add(a);
        }

        public int dfs(Vertex start){
            List<Vertex> list = new ArrayList<>();
            if (start.isVisited==true) return 0;
            else {
                Stack<Vertex> stack = new Stack<>();
                stack.push(start);
                start.isVisited=true;

               loop: while (!stack.empty()) {
//                    for (int i = 0; i < adjMatrix.length ; i++) {
//                        if (adjMatrix[stack.peek().num][i]==1 && vertexList[i].isVisited==false){
//                            stack.push(vertexList[i]);
//                            vertexList[i].isVisited=true;
//                            continue loop;
//                        }
                   for (int point : adjList[stack.peek().num]
                        ) {
                       if (vertexList[point].isVisited==false) {
                           stack.push(vertexList[point]);
                           vertexList[point].isVisited = true;
                           continue loop;
                       }
                   }
                   list.add(stack.pop());
                    }

                }
                return list.size();
            }
        }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        Graph graph = new Graph(n);

        for (int i = 0; i < p ; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            graph.addEdge(a, b);
        }

        List<Integer> countries = new ArrayList<>();
        long singles=0;

        for (int i = 0; i < n; i++) {
            if (graph.adjList[i].size()==0) singles+=1;
            else {
                countries.add(graph.dfs(graph.vertexList[i]));
//                System.out.println(countries.get(i));
            }
        }

        long answer =0;
        for (int i = 0; i < countries.size()-1; i++) {
            for (int j = i+1; j <countries.size() ; j++) {
                answer+=countries.get(i)*countries.get(j);

            }
        }
        for (int i = 0; i <countries.size(); i++) {
            answer+=singles*countries.get(i);
        }
         answer+=(singles*(singles-1))/2;

        System.out.println(answer);
    }
}