package graph.FibonacciModified;


import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        Scanner scanner = new Scanner(System.in);
        int t1 = scanner.nextInt();
        int t2 = scanner.nextInt();
        int n = scanner.nextInt();
        Fib asd= new Fib(t1, t2);

        System.out.println(asd.solve(n));
    }

    static class Fib {

        List<BigInteger> chisla;

        public Fib(int t1, int t2) {

            chisla= new ArrayList<>();
            chisla.add(0, BigInteger.valueOf((long) 0));
            chisla.add(1, BigInteger.valueOf((long) t1));
            chisla.add(2, BigInteger.valueOf((long) t2));
        }

        public BigInteger solve(int n){
            if (n==1) return chisla.get(1);
            else if (n==2) return chisla.get(2);
            else if (chisla.size()> n) return chisla.get(n);
            else {
                BigInteger ans = solve(n-2).add( solve(n-1).multiply(solve(n-1)));
                chisla.add(n, ans);
                return ans;
            }

        }
    }
}
