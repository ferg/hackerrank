package Implementation.ExtraLongFactorials;

import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        sc.close();

        BigInteger answer= BigInteger.valueOf(1);

        for (int i = 2; i <= a; i++) {
            answer= answer.multiply(BigInteger.valueOf(i));
            System.out.println(answer);
        }
        System.out.println(answer);
    }
}

