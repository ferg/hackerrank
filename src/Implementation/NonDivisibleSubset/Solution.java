package Implementation.NonDivisibleSubset;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        Set<Integer> setS= new HashSet<>();

        for (int i = 0; i < n; i++) {
            setS.add(sc.nextInt());
        }
        sc.close();

        int[] div = new int[k];

        for (int num : setS
                ) {
            div[num%k]+=1;
        }

        int answer=0;

        for (int i = 1; i <k/2 +1 ; i++) {
            if (i!=k-i) {
                answer += (div[i] > div[k - i]) ? div[i] : div[k - i];
            } else if (div[i]>0) answer+=1;
        }


        if(div[0]>0) answer+=1;

        System.out.println(answer);
    }
}