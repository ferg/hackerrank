package Implementation.QueensAttackII;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    static  class Point {
        int x;
        int y;
        boolean stop ;


        public Point(int r, int c) {
            this.x = r;
            this.y = c;
            stop = false;
        }
    }



    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int k = in.nextInt();
        int rQueen = in.nextInt();
        int cQueen = in.nextInt();
        Point queen = new Point(cQueen, rQueen);

        List<Point> line1 = new ArrayList<>();
        List<Point> line2 = new ArrayList<>();
        List<Point> line3 = new ArrayList<>();
        List<Point> line4 = new ArrayList<>();

        line1.add(queen);
        line2.add(queen);
        line3.add(queen);
        line4.add(queen);
        for(int a0 = 0; a0 < k; a0++){
            int rObstacle = in.nextInt();
            int cObstacle = in.nextInt();
            // your code goes here

            if (cQueen==cObstacle)line1.add(new Point(cObstacle, rObstacle));
            if (cQueen-rQueen==cObstacle-rObstacle)line2.add(new Point(cObstacle, rObstacle));
            if (rQueen==rObstacle)line3.add(new Point(cObstacle, rObstacle));
            if (cQueen+rQueen==cObstacle+rObstacle)line4.add(new Point(cObstacle, rObstacle));
        }
        Comparator<Point> comp = (Point a, Point b) -> {
            if (b.x==a.x) return a.y-b.y;
            else return a.x-b.x;
        };

        Collections.sort(line1, comp );
        Collections.sort(line2, comp );
        Collections.sort(line3, comp );
        Collections.sort(line4, comp );


        int sum=0;
        int num1=0,num2=0;
        int sum1=num1+num2;

        if(line1.size()==1) sum1=n-1;
        else{
            if (line1.indexOf(queen)==0) {
                num1=queen.y-1;
                num2=line1.get(line1.indexOf(queen)+1).y-queen.y-1;
                sum1= num1+num2;
            }
            else if (line1.indexOf(queen)==line1.size()-1) {
                num2=n-queen.y;
                num1=queen.y - line1.get(line1.indexOf(queen)-1).y -1;
                sum1= num1+num2;
            } else {
                num1=queen.y - line1.get(line1.indexOf(queen)-1).y -1;
                num2=line1.get(line1.indexOf(queen)+1).y-queen.y-1;
                sum1= num1+num2;
            }
        }

        int num3=0,num4=0;
        int sum2=num3+num4;

        if(line2.size()==1) sum2=queen.x>queen.y ?n+queen.y - queen.x -1: n-queen.y +queen.x -1;
        else{
            if (line2.indexOf(queen)==0) {
                num3=queen.x-1;
                num4=line2.get(line2.indexOf(queen)+1).x-queen.x-1;
                sum2= num3+num4;
            }
            else if (line2.indexOf(queen)==line2.size()-1) {
                num4=n-queen.x;
                num3=queen.x - line2.get(line2.indexOf(queen)-1).x -1;
                sum2= num3+num4;
            } else {
                num3=queen.x - line2.get(line2.indexOf(queen)-1).x -1;
                num4=line2.get(line2.indexOf(queen)+1).x-queen.x-1;
                sum2= num3+num4;
            }
        }

        int num5=0,num6=0;
        int sum3=num5+num6;

        if(line3.size()==1) sum3=n-1;
        else{
            if (line3.indexOf(queen)==0) {
                num5=queen.x-1;
                num6=line3.get(line3.indexOf(queen)+1).x-queen.x-1;
                sum3= num5+num6;
            }
            else if (line3.indexOf(queen)==line3.size()-1) {
                num6=n-queen.x;
                num5=queen.x - line3.get(line3.indexOf(queen)-1).x -1;
                sum3= num5+num6;
            } else {
                num5=queen.x - line3.get(line3.indexOf(queen)-1).x -1;
                num6=line3.get(line3.indexOf(queen)+1).x-queen.x-1;
                sum3= num5+num6;
            }
        }

        int num7=0,num8=0;
        int sum4=num7+num8;

        if(line4.size()==1) sum4= queen.x+queen.y< n ? queen.x+queen.y-2 : n*2 -queen.x-queen.y;
        else{
            if (line4.indexOf(queen)==0) {
                num7=queen.x-1;
                num8=line4.get(line4.indexOf(queen)+1).x-queen.x-1;
                sum4= num7+num8;
            }
            else if (line4.indexOf(queen)==line4.size()-1) {
                num8=n-queen.x;
                num7=queen.x - line4.get(line4.indexOf(queen)-1).x -1;
                sum4= num7+num8;
            } else {
                num7=queen.x - line4.get(line4.indexOf(queen)-1).x -1;
                num8=line4.get(line4.indexOf(queen)+1).x-queen.x-1;
                sum4= num7+num8;
            }
        }

        System.out.println(sum1+sum2+sum3+sum4);

    }
}
